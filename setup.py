from setuptools import setup, find_packages


def get_requirements(file_name):
    with open(file_name, 'r') as f:
        return f.read().splitlines()


setup(
    name='cupimcupim',
    version='0.2.0',
    python_requires='>=3.6',
    packages=find_packages(),
    install_requires=get_requirements('requirements.txt'),
    scripts=['bin/cupimcupim']
)

