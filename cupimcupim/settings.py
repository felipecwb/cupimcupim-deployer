from os import getenv

HOMEPATH   = getenv('HOME', '~')
DIRECTORY  = HOMEPATH + '/.cupimcupim'
CONFIGFILE = DIRECTORY + '/config.file.json'

BASE_DOMAIN      = getenv('CUPIMCUPIM_DOMAIN', 'localhost')
DEFAULT_REGISTRY = getenv('CUPIMCUPIM_REGISTRY', 'registry.localhost')

API_ENDPOINT = getenv('CUPIMCUPIM_ENDPOINT', 'http://cupimcupim.localhost/api')
API_TOKEN    = getenv('CUPIMCUPIM_TOKEN', '')

DOCKERFILE_PATH = getenv('CUPIMCUPIM_DOCKERFILE_PATH', 'Dockerfile')

PROJECT_SOURCE = 'git+https://bitbucket.org/felipecwb/cupimcupim-deployer.git'

