import requests
from .config import ProjectConfig
from ..settings import API_ENDPOINT, API_TOKEN

def deploy(config: ProjectConfig):
    headers = {'User-Agent': 'CupimCupim CLI'}
    if API_TOKEN:
        headers['Authorization'] = 'Token ' + API_TOKEN

    image_ref = config.image['name'] + ':' + config.image.get('tag', 'latest')

    payload = {
        'stack':   config.user,
        'service': config.name,
        'image':   image_ref,
        'command': config.command,
        'domain': config.host,
        'port':   config.port
    }

    response = requests.post(API_ENDPOINT + '/stack/deploy',
                             headers=headers,
                             json=payload)

    if response.status_code != requests.codes.ok:
        return {
            'status': False,
            'output': response.content.decode()
        }

    return response.json()

