from os import path, getcwd
from sys import exit
from .docker import build, publish
from .trigger import deploy
from .config import list_projects, \
                    ProjectConfig, InvalidDirectory, ProjectNotFound


class ProjectCommands:
    """
    Project management
    """
    def __init__(self, name=None):
        if not name:
            name = path.basename(getcwd())

        self._name = name

    def list(self):
        projects = list_projects()
        print('Projects:', '' if len(projects) else 'no projects found')
        for project in projects:
            print(' ->', project)

    def create(self, **config):
        try:
            project_config = ProjectConfig(self._name)
            project_config.create(**config)
        except InvalidDirectory as e:
            print('ERROR:', e.args)
            return exit(250)

        return self.show()

    def show(self):
        try:
            config = ProjectConfig.load(self._name)

            print('CupimCupim:')
            print(' - Project:', config.name)
            print(' - Username:', config.user)
            print(' - Host:', config.host)
            if config.host:
                print(' - Link:', 'http://' + config.host + '/')
            print(' - Container Port:', config.port)
            print(' - Project Path:', config.path)
            print(' - Project Dockerfile:', config.dockerfile)
            print(' - Image:', config.image['name'])
            print(' - Image Tag:', config.image['tag'])
            print(' - Command:', config.command)
            print(' - Environment:', config.environment)
            print(' - Environment Variables:')
            for name, value in config.envvars.items():
                print('  |-', name, '=', value)

        except (ProjectNotFound, Exception) as e:
            print('ERROR:', e.args[0])
            return exit(250)

    def remove(self):
        try:
            config = ProjectConfig.load(self._name)
            config.drop()

            print('Project', project, 'removed!')

        except (ProjectNotFound, Exception) as e:
            print('ERROR:', e.args[0])
            return exit(250)

    def set_image(self, name, tag='latest'):
        try:
            config = ProjectConfig.load(self._name)
            config.set_image(name, tag)
            config.save()

            print('Project', self._name, 'set image to:', name + ':' + tag)

        except (ProjectNotFound, Exception) as e:
            print('ERROR:', e.args[0])
            return exit(250)

    def set_environment(self, environment=None):
        try:
            config = ProjectConfig.load(self._name)
            config.set_environment(environment)
            config.save()

            print('Project', self._name, 'set to', environment, 'environment!')

        except (ProjectNotFound, Exception) as e:
            print('ERROR:', e.args[0])
            return exit(250)

    def set_command(self, command=None):
        try:
            config = ProjectConfig.load(self._name)
            config.set_command(command)
            config.save()

            print('Project', self._name, 'set command:', command)

        except (ProjectNotFound, Exception) as e:
            print('ERROR:', e.args[0])
            return exit(250)

    def set_connection(self, host=None, port=None):
        try:
            if host and not port:
                port = 80

            config = ProjectConfig.load(self._name)
            config.set_connection(host, port)
            config.save()

            connection = 'None'
            if host and port:
                connection = host + ':' + str(port)

            print('Project', self._name, 'set connection:', connection)

        except (ProjectNotFound, Exception) as e:
            print('ERROR:', e.args[0])
            return exit(250)

    def env_add(self, name=None, value=None, **envvars):
        if name and value:
            envvars[name] = value

        try:
            config = ProjectConfig.load(self._name)
            for name, value in envvars.items():
                config.add_env(name, value)

            config.save()

            print('Project:', self._name)
            print('env variable added: ')
            for name, value in envvars.items():
                print(' -', name, '=', value)

        except (ProjectNotFound, Exception) as e:
            print('ERROR:', e.args[0])
            return exit(250)

    def env_remove(self, name=None, **envvars):
        if name:
            envvars[name] = True

        try:
            config = ProjectConfig.load(self._name)

            for name, value in envvars.items():
                config.remove_env(name)

            config.save()

            print('Project:', self._name)
            print('env variable removed: ')
            for name, value in envvars.items():
                print(' -', name, '=', value)

        except (ProjectNotFound, Exception) as e:
            print('ERROR:', e.args[0])
            return exit(250)


    def build(self, name=None):
        try:
            name = name if name else self._name
            config = ProjectConfig.load(name)

            status = build(config)

            if not status:
                print('\nCupimCupim: failed to build project')
                return exit(150)

            print('\nCupimCupim: build successful')
            print(' - name:', status['name'])
            print(' - tag:', status['tag'])
            print(' - images:', status['images'])

        except (ProjectNotFound, Exception) as e:
            print('ERROR:', e.args[0])
            return exit(250)

    def deploy(self, name=None):
        try:
            name = name if name else self._name
            config = ProjectConfig.load(name)

            print('CupimCupim: -> publish image on registry...')
            status = publish(config)

            if not status:
                print('\nCupimCupim: failed to publish images')
                return exit(150)

            print('\nCupimCupim: publish successful')

            # trigger deploy to stack
            print('CupimCupim: -> trigger deploy...')
            result = deploy(config)

            if not result['status']:
                print('ERROR: deploy trigger failed...\n')
                print(result['output'])
                return exit(100)

            print('CupimCupim: deploy trigger success')
            print(result['output'])

            print('\nCupimCupim: wait some seconds and try access: ')
            print(' - Link:', 'http://' + config.host + '/')

        except (ProjectNotFound, Exception) as e:
            print('ERROR:', e.args[0])
            return exit(250)

