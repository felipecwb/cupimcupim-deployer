import subprocess
from datetime import datetime
from .config import ProjectConfig

def build(config: ProjectConfig):
    path_dockerfile = config.path + '/' + config.dockerfile
    tag         = datetime.now().strftime('%m%d.%H%M')
    imageTag    = config.image['name'] + ':' + tag
    imageLatest = config.image['name'] + ':latest'

    command = ['docker', 'build', config.path,
               '-f', path_dockerfile,
               '-t', imageLatest,
               '-t', imageTag]

    if config.environment:
        command.extend([
            '--build-arg',
            'ENVIRONMENT={}'.format(config.environment)
        ])

    if len(config.envvars) > 0:
        envvars_string = '\\n'.join(
            ['{}=\"{}\"'.format(key, value.replace('\"', '\\"')
                                          .replace('\'', '\'\\\'\''))
             for key, value in config.envvars.items()]
        )

        command.extend([
            '--build-arg',
            'ENVVARS=\'{}\''.format(envvars_string)
        ])

    returncode = subprocess.call(' '.join(command), shell=True)

    if returncode != 0:
        return False

    config.update_tag(tag)
    config.save()

    return {
        'name': config.name,
        'images': [imageTag, imageLatest],
        'tag': tag
    }


def publish(config: ProjectConfig):
    imageLatest = config.image['name'] + ':latest'
    imageTag    = config.image['name'] + ':' + config.image['tag']

    command = ['docker', 'push', imageTag]
    returncode = subprocess.call(' '.join(command), shell=True)

    if returncode != 0:
        return False


    command[2] = imageLatest
    returncode = subprocess.call(' '.join(command), shell=True)

    if returncode != 0:
        return False

    return True

