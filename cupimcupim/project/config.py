from os import path, getcwd, walk, remove
from json import load, dump
from ..user.config import get_all as user_config
from ..settings import DIRECTORY, CONFIGFILE, DOCKERFILE_PATH


class InvalidDirectory(Exception):
    """Error on invalid directory"""
    pass


class ProjectNotFound(Exception):
    """Error on invalid directory"""
    pass


class ProjectConfig:
    @staticmethod
    def get_filepath(name):
        return '{}/{}.project.json'.format(DIRECTORY, name)

    @staticmethod
    def load(name):
        filename = ProjectConfig.get_filepath(name)
        if not path.isfile(filename):
            raise ProjectNotFound('project {} not found!'.format(name))


        data = load(open(filename))
        config = ProjectConfig(name)

        if 'name' not in data:
            raise Exception('config malformed error.')

        config.name = data.get('name')
        config.user = data.get('user')
        config.path = data.get('path')
        config.host = data.get('host')
        config.port = data.get('port')
        config.dockerfile  = data.get('dockerfile')
        config.image       = data.get('image')
        config.command     = data.get('command')
        config.environment = data.get('environment')
        config.envvars     = data.get('envvars')

        return config

    def __init__(self, name):
        user = user_config()

        self.name = name
        self.user = user['username']

        self.host = '{}.{}'.format(self.name, user['subdomain'])
        self.port = 80

        self.path = getcwd()
        self.dockerfile = DOCKERFILE_PATH

        self.image = {
            'name': '{}/{}/{}'.format(user['registry'],
                                      user['username'],
                                      name),
            'tag': None,
        }

        self.command = None

        self.environment = None
        self.envvars = {}

    def create(self, **config):
        if 'host' in config:
            self.host = config['host']

        if 'port' in config:
            self.port = config['port']

        if 'path' in config:
            if not path.isdir(config['path']):
                raise InvalidDirectory(
                    'directory {} invalid!'.format(config['path'])
                )

            self.path = config['path']

        if 'image' in config:
            self.image['name'] = config['image']

        if 'dockerfile' in config:
            self.dockerfile = config['dockerfile']

        if 'command' in config:
            self.command = config['command']

        if 'environment' in config:
            self.environment = config['environment']

        return self.save()

    def drop(self):
        filepath = ProjectConfig.get_filepath(self.name)
        remove(filepath)
        return True

    def set_image(self, name, tag=None):
        self.image['name'] = name
        if tag:
            self.image['tag'] = tag

        return self

    def set_connection(self, host=None, port=None):
        self.host = host
        self.port = port

        return self

    def set_environment(self, environment):
        self.environment = environment
        return self

    def set_command(self, command):
        self.command = command
        return self

    def add_env(self, name, value):
        self.envvars[name] = value
        return self

    def remove_env(self, name):
        if name not in self.envvars:
            raise Exception('variable {} not found.'.format(name))

        del self.envvars[name]
        return self

    def update_tag(self, new_tag):
        self.image['tag'] = new_tag
        return self

    def save(self):
        filepath = ProjectConfig.get_filepath(self.name)
        dump(self.__dict__, open(filepath, 'w+'))
        return True


def list_projects():
    projects = []

    for root, dirs, files in walk(DIRECTORY):
        projects = files

    projects.remove(path.basename(CONFIGFILE))
    return [project.replace('.project.json', '') for project in projects]

