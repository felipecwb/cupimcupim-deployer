from os import path, mkdir
from json import load, dump
from ..settings import DIRECTORY, CONFIGFILE


def create(config):
    if not path.isdir(DIRECTORY):
        mkdir(DIRECTORY)

    dump(config, open(CONFIGFILE, 'w+'))
    return True


def get_all():
    if not path.isfile(CONFIGFILE):
        raise Exception('User configuration not found.')

    return load(open(CONFIGFILE, 'r+'))
