import subprocess
from sys import exit
from getpass import getpass
from . import config
from ..settings import DEFAULT_REGISTRY, BASE_DOMAIN


class UserCommands:
    """
    User information management
    """
    def create(self):
        print("Welcome to CupimCupim!")
        print("======================")
        print("\nLet`s get start with your account on registry service.")
        print(" -> http://{}/users/sign_up\n".format(DEFAULT_REGISTRY))

        user_config = dict(
            domain=BASE_DOMAIN,
            registry=DEFAULT_REGISTRY
        )

        try:
            user_config['username'] = input("I will need your username you created: ")
            password = getpass("Your password: ")
        except KeyboardInterrupt as e:
            print("\n\n - dont kill me!\n")
            return exit(200)

        print("\nLet`s login on registry:")

        command = ['docker',
                   'login',
                   '-u',
                   user_config['username'],
                   '--password-stdin',
                   DEFAULT_REGISTRY]

        proc = subprocess.Popen(command,
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)

        output = proc.communicate(password.encode('utf-8'), timeout=30)[0]

        print(output.decode('utf-8'))

        if proc.returncode != 0:
            print("\nFailed to login on registry.")
            print("- check the username & password.")
            print("- check if SSL/TLS works.")
            print("and retry.\n")
            return exit(150)

        user_config['subdomain'] = '{}.{}'.format(
            user_config['username'],
            BASE_DOMAIN
        )

        config.create(user_config)

        print("\nAll right here!")


    def show(self):
        print("CupimCupim:")

        try:
            user_config = config.get_all()

            print(" -> Domain: {}".format(user_config['domain']))
            print(" -> Registry: {}".format(user_config['registry']))
            print(" -> Username: {}".format(user_config['username']))
            print(" -> Sub.Domain: {}".format(user_config['subdomain']))

        except Exception as e:
            print(" -x ERROR: No configfile found!")
            return exit(250)

