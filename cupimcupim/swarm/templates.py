
STACK = """
version: '3.7'

networks:
  default:
    driver: overlay
{networks_def}

services:
  {service}:
    image: {image}
{command_def}
    networks:
      - default
{networks_list}
    deploy:
      mode: replicated
      replicas: 1
      resources:
        limits:
          cpus: '1'
          memory: '1024M'
      update_config:
        parallelism: 2
        delay: 10s
        order: stop-first
{deploy_labels}
"""

COMMAND_DEF = "    command: {command}"

NETWORK_DEF = "  {name}:\n    external: true"

NETWORK_LIST = "      - {name}"

LABELS = """      labels:
        - "traefik.enable={enable}"
        - "traefik.port={port}"
        - "traefik.docker.network={network}"
        - "traefik.frontend.rule=Host:{domain}"
        - "traefik.frontend.entryPoints=http,https"
"""
