from os import getenv

TRAEFIK_NETWORK = getenv('TRAEFIK_NETWORK', 'frontend')
BACKEND_NETWORK = getenv('BACKEND_NETWORK', 'backend')
STACK_NETWORKS = [TRAEFIK_NETWORK, BACKEND_NETWORK]

STACK_PREFIX = getenv('STACK_PREFIX', 'cupimcupim-')

