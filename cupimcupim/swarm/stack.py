import subprocess
from .settings import \
    TRAEFIK_NETWORK, \
    STACK_NETWORKS, \
    STACK_PREFIX


def _build_definition(**info):
    from .templates import \
        STACK, \
        COMMAND_DEF, \
        NETWORK_DEF, \
        NETWORK_LIST, \
        LABELS

    command  = ''
    if info.get('command', None):
        command = COMMAND_DEF.format(command=info.get('command'))

    net_def  = ''
    net_list = ''
    if 'networks' in info:
        net_def = '\n'.join([NETWORK_DEF.format(name=name)
                             for name in info['networks']])

        net_list = '\n'.join([NETWORK_LIST.format(name=name)
                              for name in info['networks']])

    labels = ''
    if info.get('domain') and info.get('port'):
        labels = LABELS.format(enable='true',
                               network=TRAEFIK_NETWORK,
                               domain=info['domain'],
                               port=info['port'])

    return STACK.format(service=info['service'],
                        image=info['image'],
                        command_def=command,
                        networks_def=net_def,
                        networks_list=net_list,
                        deploy_labels=labels)


def deploy(stack, service, image, command=None, domain=None, port=None):
    command_line = ['docker',
                    'stack',
                    'deploy',
                    STACK_PREFIX + stack,
                    '--with-registry-auth',
                    '--compose-file',
                    '-']

    definition = _build_definition(service=service,
                                   image=image,
                                   command=command,
                                   domain=domain,
                                   port=port,
                                   networks=STACK_NETWORKS,
                                   traefik_net=TRAEFIK_NETWORK)

    process = subprocess.Popen(command_line,
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    output, errput = process.communicate(definition.encode('utf-8'))

    content = output.decode('utf-8')
    if process.returncode != 0:
        content = errput.decode('utf-8')

    return process.returncode == 0, content

