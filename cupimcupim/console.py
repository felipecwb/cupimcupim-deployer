from fire import Fire
from .user.console import UserCommands
from .project.console import ProjectCommands
from .server.console import ServerCommands


class Commands:
    """
    Project:
      ____ _   _ ____ ___ __  __  ____ _   _ ____ ___ __  __
     / ___| | | |  _ \_ _|  \/  |/ ___| | | |  _ \_ _|  \/  |
    | |   | | | | |_) | || |\/| | |   | | | | |_) | || |\/| |
    | |___| |_| |  __/| || |  | | |___| |_| |  __/| || |  | |
     \____|\___/|_|  |___|_|  |_|\____|\___/|_|  |___|_|  |_|
    """
    def __init__(self):
        self.project = ProjectCommands()
        self.user = UserCommands()
        self.server = ServerCommands()


def run():
    return Fire(Commands, name="cupimcupim")
