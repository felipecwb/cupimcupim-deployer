import random
from sanic import Blueprint, response
from .settings import STATIC_FOLDER, API_TOKEN, REGISTRY_CERTIFICATE


site = Blueprint('site')
site.static('/static', STATIC_FOLDER, name='static')
site.static('/favicon.ico', STATIC_FOLDER + '/images/logo-favicon.ico')


@site.route('/')
async def index(request):
    return response.html("""
        <html>
            <head>
                <link rel="shortcut icon" href="/static/images/logo-favicon.ico" />
                <title>CupimCupim</title>
                <style>
                    .logo-center { position: absolute;
                                   top:50%;
                                   left:50%;
                                   margin-top:-57px;
                                   margin-left:-313; }
                </style>
            </head>
            <body>
                <img class="logo-center" src="/static/images/logo-official.png"/>
            </body>
        </html>
    """)


@site.get('/install.sh')
async def install_script(request):
    from ..settings import \
        BASE_DOMAIN, \
        DEFAULT_REGISTRY, \
        DOCKERFILE_PATH, \
        API_ENDPOINT, \
        PROJECT_SOURCE

    registry_script = 'echo \'...nothing setup\''
    if REGISTRY_CERTIFICATE:
        registry_script = 'sudo mkdir -vp /etc/docker/certs.d/{registry}/\n' \
            + 'echo -n \'{content}\'' \
            + ' | sudo tee /etc/docker/certs.d/{registry}/ca.crt'

        registry_script = registry_script.format(
            registry=DEFAULT_REGISTRY,
            content=open(REGISTRY_CERTIFICATE, 'r').read()
        )

    shell_script = """#!/usr/bin/env bash

echo '>> apt-get install python3-pip:'
sudo apt-get install -yy python3-pip

echo '>> Install registry certificate:'
{registry_script}

echo '>> INSTALL cupimcupim'
sudo pip3 install --upgrade {project_source}

echo ''
echo '>> Add config to .cupimcupim.bash:'
{{ \\
    echo ''; \\
    echo '# cupimcupim environments'; \\
    echo 'export CUPIMCUPIM_DOMAIN="{domain}"'; \\
    echo 'export CUPIMCUPIM_REGISTRY="{registry}"'; \\
    echo 'export CUPIMCUPIM_DOCKERFILE_PATH="{dockerfile}"'; \\
    echo 'export CUPIMCUPIM_ENDPOINT="{endpoint}"'; \\
    echo 'export CUPIMCUPIM_TOKEN="{token}"'; \\
    echo ''; \\
    cupimcupim -- --completion; \\
}} | tee $HOME/.cupimcupim.bash

if [ -z "$(grep cupimcupim $HOME/.bashrc)" ]; then
    echo '>> append source .cupimcupim.bash to .bashrc';
    echo 'source $HOME/.cupimcupim.bash' >> $HOME/.bashrc
fi

exec $SHELL

echo '>> restart SHELL with: exec $SHELL'
echo -e '>> and try: cupimcupim\\n'
"""

    shell_script = shell_script.format(
        domain=BASE_DOMAIN,
        registry=DEFAULT_REGISTRY,
        dockerfile=DOCKERFILE_PATH,
        endpoint=API_ENDPOINT,
        token=API_TOKEN,
        registry_script=registry_script,
        project_source=PROJECT_SOURCE
    )

    return response.text(shell_script)

