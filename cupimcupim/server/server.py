from sanic import Sanic
from .api import api
from .site import site
from .settings import SERVER_HOST, SERVER_PORT, SERVER_DEBUG


def run():
    app = Sanic('cupimcupim')

    app.blueprint(site)
    app.blueprint(api)

    app.run(
        host=SERVER_HOST,
        port=SERVER_PORT,
        access_log=True,
        debug=SERVER_DEBUG,
        auto_reload=SERVER_DEBUG
    )

