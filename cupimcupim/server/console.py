from .server import run

class ServerCommands:
    """
    Server Agent processing
    """
    def run(self):
        run()
