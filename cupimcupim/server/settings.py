from os import getenv, path

SERVER_HOST  = getenv('SERVER_HOST', '0.0.0.0')
SERVER_PORT  = int(getenv('SERVER_PORT', 8000))
SERVER_DEBUG = bool(getenv('SERVER_DEBUG'))

API_TOKEN = getenv('API_TOKEN', '')

STATIC_FOLDER = getenv('SERVER_STATIC_FOLDER',
                       path.abspath(path.dirname(__file__) + '/../../static'))

REGISTRY_CERTIFICATE = getenv('REGISTRY_CERTIFICATE', None)

