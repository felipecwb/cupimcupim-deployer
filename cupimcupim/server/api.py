import json
from sanic import Blueprint, response
from ..swarm import stack, settings
from .settings import API_TOKEN


api = Blueprint('api', url_prefix='/api')


@api.middleware('request')
async def verify_token(request):
    if not request.path.startswith(api.url_prefix + '/'):
        return

    request_token = request.headers.get('authorization', '')

    if API_TOKEN and API_TOKEN != request_token.strip()[6:]:
        return response.json({
            'status': False,
            'message': 'please, verify your authorization token.',
        }, 401)


@api.get('/')
async def index(request):
    return response.json({
        'status': True,
        'message': 'all working... I guess!'
    })


@api.post('/stack/deploy')
async def stack_deploy(request):
    required_fields = set(('stack',
                           'service',
                           'image'))

    if not request.json:
        return response.json({
            'status': False,
            'message': 'empty json payload!',
        })

    if not required_fields.issubset(request.json):
        return response.json({
            'status': False,
            'message': 'missing required fields!',
            'fields': required_fields
        })

    data = request.json

    status, output = stack.deploy(
        data.get('stack'),
        data.get('service'),
        data.get('image'),
        data.get('command', None),
        data.get('domain', None),
        data.get('port', None)
    )

    return response.json({
        'status': status,
        'output': output
    })

