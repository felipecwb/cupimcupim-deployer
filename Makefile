#.PHONY: install

deps:
	pip install -r requirements.txt

test:
	@echo "Tests!?"
	@echo "see: https://bit.ly/2TdH2tg"

install: deps test
	pip install .

