FROM alpine:3.9

ENV GUID    1000
ENV USER    cupimcupim
ENV GROUP   cupimcupim
ENV WORKDIR /home/cupimcupim

ENV SERVER_STATIC_FOLDER $WORKDIR/source/static

# USER
RUN addgroup -g $GUID $GROUP \
    && adduser -S -u $GUID -G $GROUP $USER

RUN apk add --update --no-cache \
        docker \
        gcc \
        make \
        libc-dev \
        python3 \
        python3-dev

RUN pip3 install --upgrade pip

WORKDIR $WORKDIR
RUN mkdir -v source/ .docker/

# dependencies
COPY requirements.txt source/requirements.txt
RUN pip install -r source/requirements.txt

# code install
COPY . source/
RUN make install -C source/

USER $USER

EXPOSE 8000

CMD /usr/bin/cupimcupim server run
