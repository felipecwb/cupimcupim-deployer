
![cupim](static/images/logo-official.png)

Project to deploy and test projects with docker swarm and traefik router.

## Instalation

You will need: `docker`, `git`, `python3`, `pip3`  

**script with `apt-get install python3-pip`:**
```sh
curl http://cupimcupim.server.app/install.sh | bash -
```

_change: `cupimcupim.server.app`: is the application server_  


## Swarm Service description sample:

```yaml
version: '3.7'

networks:
  frontend:
    external: true

configs:
  docker_client:
    file: $HOME/.docker/config.json

secrets:
  docker_ca:
    file: ./certs/docker/tls-ca.pem
  docker_private_key:
    file: ./certs/docker/tls-client-key.pem
  docker_certificate:
    file: ./certs/docker/tls-client-cert.pem
  registry_certificate:
    file: ./certs/registry.localhost.crt

services:
  server:
    image: registry.localhost/cupimcupim:latest
    environment:
      DOCKER_HOST: "tcp://0.0.0.0:2376"
      DOCKER_CERT_PATH: "/run/secrets"
      DOCKER_TLS_VERIFY: "1"

      API_TOKEN: "55224351f2417ec4a7d337ccc48d0ebb2d94cc81"

      STACK_PREFIX: "cupimcupim-"
      TRAEFIK_NETWORK: "frontend"
      BACKEND_NETWORK: "backend"

      REGISTRY_CERTIFICATE: "/run/secrets/registry.crt"

      CUPIMCUPIM_DOMAIN: "localhost"
      CUPIMCUPIM_REGISTRY: "registry.localhost"
      CUPIMCUPIM_ENDPOINT: "http://cupimcupim.localhost/api"
      CUPIMCUPIM_DOCKERFILE_PATH: "Dockerfile"
    configs:
      - source: docker_client
        target: /home/cupimcupim/.docker/config.json
    secrets:
      - source: docker_ca
        target: ca.pem
      - source: docker_certificate
        target: cert.pem
      - source: docker_private_key
        target: key.pem
      - source: registry_certificate
        target: registry.crt
    networks:
      - frontend
    deploy:
      mode: replicated
      replicas: 1
      update_config:
        parallelism: 2
        delay: 10s
        order: start-first
      labels:
        - "traefik.enable=true"
        - "traefik.port=8000"
        - "traefik.docker.network=frontend"
        - "traefik.frontend.rule=Host:cupimcupim.localhost,localhost,0.0.0.0"
        - "traefik.frontend.entryPoints=http,https"

```

